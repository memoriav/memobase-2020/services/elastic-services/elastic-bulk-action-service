/*
 * Copyright (C) 2020 - present Jonas Waeber
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package ch.memobase

import ch.memobase.settings.SettingsLoader
import org.apache.logging.log4j.LogManager
import kotlin.system.exitProcess


class Service(fileName: String) {
    companion object {
        const val REPORTING_STEP_NAME_PROPERTY = "reportingStepName"
        const val ELASTIC_INDEX_NAME_PROPERTY = "elastic.indexName"
        const val ELASTIC_HOST_LIST_PROPERTY = "elastic.hosts"
        const val ELASTIC_API_KEY_ID_PROPERTY = "elastic.apiKeyId"
        const val ELASTIC_API_KEY_SECRET_PROPERTY = "elastic.apiKeySecret"
        const val ELASTIC_CREATE_DOCUMENTS_PROPERTY = "elastic.createDocuments"
        const val ELASTIC_INDEX_MAPPING_FILE_PATH = "elastic.indexMappingFilePath"
        const val ELASTIC_INDEX_SETTING_FILE_PATH = "elastic.indexSettingFilePath"
        const val ELASTIC_CA_CERT_PATH = "elastic.caCertPath"
        const val ELASTIC_INDEX_LOAD_MAPPING = "elastic.indexLoadMapping"
        const val ELASTIC_USE_INGEST_PIPELINE = "elastic.useIngestPipeline"
        const val ENVIRONMENT = "environment"
        const val APP_VERSION = "appVersion"


        const val KAFKA_HEADER_KEY_RECORD_SET_ID = "recordSetId"

        fun getRandomReportId(): String {
            val allowedChars = ('A'..'Z') + ('a'..'z') + ('0'..'9')
            return (1..10).map { allowedChars.random() }.joinToString("")
        }

        fun loadSettings(fileName: String): SettingsLoader {
            return SettingsLoader(
                listOf(
                    ELASTIC_INDEX_NAME_PROPERTY,
                    ELASTIC_HOST_LIST_PROPERTY,
                    ELASTIC_API_KEY_ID_PROPERTY,
                    ELASTIC_API_KEY_SECRET_PROPERTY,
                    REPORTING_STEP_NAME_PROPERTY,
                    ELASTIC_CREATE_DOCUMENTS_PROPERTY,
                    ELASTIC_INDEX_MAPPING_FILE_PATH,
                    ELASTIC_INDEX_SETTING_FILE_PATH,
                    ELASTIC_INDEX_LOAD_MAPPING,
                    ELASTIC_CA_CERT_PATH,
                    ELASTIC_USE_INGEST_PIPELINE,
                    ENVIRONMENT,
                    APP_VERSION,
                ), fileName, useConsumerConfig = true, useProducerConfig = true
            )
        }
    }

    private val log = LogManager.getLogger(this::class.java)

    private val settings = loadSettings(fileName)

    private val es = settings.appSettings
    private val version = es.getProperty(APP_VERSION)
    private val step = es.getProperty(REPORTING_STEP_NAME_PROPERTY)
    private val consumer = Consumer(settings.inputTopic, settings.kafkaConsumerSettings)
    private val producer = Producer(settings.outputTopic, step, version, settings.kafkaProducerSettings)

    private val indexName = es.getProperty(ELASTIC_INDEX_NAME_PROPERTY)
    private val indexLoadMapping = try {
        es.getProperty(ELASTIC_INDEX_LOAD_MAPPING).toBooleanStrict()
    } catch (ex: IllegalArgumentException) {
        log.error("Invalid value for $ELASTIC_INDEX_LOAD_MAPPING: ${es.getProperty(ELASTIC_INDEX_LOAD_MAPPING)}")
        exitProcess(1)
    }
    private val useIngestPipeline = try {
        es.getProperty(ELASTIC_USE_INGEST_PIPELINE).toBooleanStrict()
    } catch (ex: IllegalArgumentException) {
        log.error("Invalid value for $ELASTIC_USE_INGEST_PIPELINE: ${es.getProperty(ELASTIC_USE_INGEST_PIPELINE)}")
        exitProcess(1)
    }
    private val indexMappingFilePath = es.getProperty(ELASTIC_INDEX_MAPPING_FILE_PATH)
    private val indexSettingFilePath = es.getProperty(ELASTIC_INDEX_SETTING_FILE_PATH)
    private val createDocuments = try {
        es.getProperty(ELASTIC_CREATE_DOCUMENTS_PROPERTY).toBooleanStrict()
    } catch (ex: IllegalArgumentException) {
        log.error(
            "Invalid value for $ELASTIC_CREATE_DOCUMENTS_PROPERTY: ${
                es.getProperty(
                    ELASTIC_CREATE_DOCUMENTS_PROPERTY
                )
            }"
        )
        exitProcess(1)
    }
    private val hosts = es.getProperty(ELASTIC_HOST_LIST_PROPERTY)
    private val apiKeyId = es.getProperty(ELASTIC_API_KEY_ID_PROPERTY)
    private val apiKeySecret = es.getProperty(ELASTIC_API_KEY_SECRET_PROPERTY)
    private val caCertPath = es.getProperty(ELASTIC_CA_CERT_PATH)

    private val elastic = ElasticsearchIndex(
        hosts,
        apiKeyId,
        apiKeySecret,
        indexLoadMapping,
        indexMappingFilePath,
        indexSettingFilePath,
        caCertPath,
        indexName,
        useIngestPipeline,
        createDocuments,
        es.getProperty(ENVIRONMENT),
    )
    private val processor = MessageProcessor(
        consumer,
        producer,
        elastic,
    )

    init {
        log.info("App Version: $version")
        log.info("Reporting Name: $step.")
        log.info("Consuming from topic: ${settings.inputTopic}.")
        log.info("Client Id: ${settings.kafkaConsumerSettings.getProperty("client.id")}.")
        consumer.subscribe()
        log.info("Subscribed to topics. Starting process.")
        while (true) {
            processor.process()
        }
    }
}

