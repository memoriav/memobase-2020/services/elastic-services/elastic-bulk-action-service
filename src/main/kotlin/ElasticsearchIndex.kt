/*
 * Copyright (C) 2020 - present Jonas Waeber
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package ch.memobase

import ch.memobase.Service.Companion.KAFKA_HEADER_KEY_RECORD_SET_ID
import ch.memobase.reporting.ReportStatus
import co.elastic.clients.elasticsearch.ElasticsearchClient
import co.elastic.clients.elasticsearch._types.ElasticsearchException
import co.elastic.clients.elasticsearch._types.HealthStatus
import co.elastic.clients.elasticsearch.core.BulkRequest
import co.elastic.clients.elasticsearch.core.BulkResponse
import co.elastic.clients.elasticsearch.core.bulk.BulkOperation
import co.elastic.clients.elasticsearch.core.bulk.BulkResponseItem
import co.elastic.clients.elasticsearch.indices.CreateIndexRequest
import co.elastic.clients.elasticsearch.indices.ExistsRequest
import co.elastic.clients.json.JsonData
import co.elastic.clients.json.JsonpMappingException
import co.elastic.clients.json.jackson.JacksonJsonpMapper
import co.elastic.clients.transport.ElasticsearchTransport
import co.elastic.clients.transport.rest_client.RestClientTransport
import org.apache.http.message.BasicHeader
import org.apache.logging.log4j.LogManager
import org.apache.logging.log4j.message.ObjectMessage
import org.elasticsearch.client.RestClient
import java.io.File
import java.io.IOException
import java.net.SocketTimeoutException
import java.nio.charset.Charset
import java.nio.charset.StandardCharsets
import java.util.*
import kotlin.math.pow
import kotlin.system.exitProcess


class ElasticsearchIndex(
    hosts: String, apiKeyId: String, apiKeySecret: String,
    indexLoadMapping: Boolean,
    indexMappingFilePath: String,
    indexSettingFilePath: String,
    certPath: String,
    private val index: String,
    private val useIngestPipeline: Boolean,
    private val createDocuments: Boolean,
    private val environment: String,
) {
    private val log = LogManager.getLogger(this::class.java)
    private val client: ElasticsearchClient

    companion object {
        private const val ELASTICSEARCH_PIPELINE_NAME_PREFIX = "memobase-update-"
    }

    init {
        client = createClient(hosts, apiKeyId, apiKeySecret, certPath)
        checkHealth()
        checkIndex(index, indexMappingFilePath, indexSettingFilePath, indexLoadMapping)
    }

    private fun createClient(
        hosts: String, apiKeyId: String, apiKeySecret: String, certPath: String,
    ): ElasticsearchClient {
        val apiKeyAuth =
            Base64.getEncoder().encodeToString(("$apiKeyId:$apiKeySecret").toByteArray(StandardCharsets.UTF_8))
        val parsedHosts = try {
            parseHost(hosts)
        } catch (ex: InvalidHostException) {
            log.error("Invalid host: ${ex.message}")
            exitProcess(1)
        }
        val restClientBuilder = RestClient.builder(*parsedHosts)
            .setDefaultHeaders(arrayOf(BasicHeader("Authorization", "ApiKey $apiKeyAuth")))
            .setRequestConfigCallback {
                it.setSocketTimeout(60000)
            }
        restClientBuilder.setHttpClientConfigCallback {
            if (certPath.isNotEmpty()) {
                val sslContext = buildSSLContext(certPath)
                it.setSSLContext(sslContext)
            }
            it
        }


        val transport: ElasticsearchTransport = RestClientTransport(restClientBuilder.build(), JacksonJsonpMapper())
        return ElasticsearchClient(transport)
    }

    private fun checkHealth() {
        try {
            val response = client.cluster().health {
                it.waitForStatus(HealthStatus.Green)
            }
            log.info("Elasticsearch cluster health: ${response.status()}")
        } catch (ex: ElasticsearchException) {
            log.error("Elasticsearch cluster health failed: ${ex.localizedMessage}")
            exitProcess(1)
        }
    }

    private fun checkIndex(
        indexName: String,
        indexMappingFilePath: String,
        indexSettingFilePath: String,
        indexLoadMapping: Boolean,
    ) {
        try {
            val indexExistsResponse = client.indices().exists(
                ExistsRequest.Builder().index(indexName).build()
            )
            if (indexExistsResponse.value()) {
                log.info("Index $indexName exists.")
            } else {
                log.info("Index $indexName does not exist. Creating it now!")
                createIndex(indexName, indexMappingFilePath, indexSettingFilePath, indexLoadMapping)
            }
        } catch (ex: ElasticsearchException) {
            log.error("Error checking index: ${ex.localizedMessage}")
            exitProcess(1)
        }
    }

    private fun createIndex(
        indexName: String,
        indexMappingFilePath: String,
        indexSettingFilePath: String,
        indexLoadMapping: Boolean,
    ) {
        try {
            val response = client.indices().create(
                CreateIndexRequest.Builder()
                    .index(indexName)
                    .mappings { mappingBuilder ->
                        if (indexLoadMapping) {
                            try {
                                val indexMapping = File(indexMappingFilePath).inputStream()
                                mappingBuilder.withJson(indexMapping)
                            } catch (ex: IOException) {
                                log.error("Error loading index mapping: ${ex.localizedMessage}")
                                exitProcess(1)
                            } catch (ex: JsonpMappingException) {
                                log.error("Error parsing index mapping: ${ex.localizedMessage}")
                                exitProcess(1)
                            }
                        }
                        mappingBuilder
                    }
                    .settings { settingsBuilder ->
                        if (indexLoadMapping) {
                            try {
                                val indexSettings = File(indexSettingFilePath).inputStream()
                                settingsBuilder.withJson(indexSettings)
                            } catch (ex: IOException) {
                                log.error("Error loading index settings: ${ex.localizedMessage}")
                                exitProcess(1)
                            } catch (ex: JsonpMappingException) {
                                log.error("Error parsing index settings: ${ex.localizedMessage}")
                                exitProcess(1)
                            }
                        }
                        settingsBuilder
                    }
                    .build()
            )
            if (response.acknowledged()) {
                log.info("Index $indexName created.")
            } else {
                log.error("Index $indexName creation failed.")
                exitProcess(1)
            }
        } catch (e: ElasticsearchException) {
            log.error("Error creating index: ${e.localizedMessage}")
            exitProcess(1)
        }
    }

    fun bulkIngest(messages: List<MessageData>): List<IndexResponseReport> {
        val bulkRequest = BulkRequest.Builder()
            .index(index)
            .operations(messages.map { message ->
                val builder = BulkOperation.Builder()
                if (createDocuments) {
                    // create is needed when writing to a data stream in elasticsearch.
                    builder.create { create ->
                        create.document(JsonData.from(message.value.reader()))
                    }
                } else {
                    val recordSetId =
                        message.headers.lastHeader(KAFKA_HEADER_KEY_RECORD_SET_ID)?.value()
                            ?.toString(Charset.defaultCharset())
                    builder.index { index ->
                        index.id(message.key)
                            .document(JsonData.from(message.value.reader()))
                        if (recordSetId != null && useIngestPipeline) {
                            index
                                .pipeline("$ELASTICSEARCH_PIPELINE_NAME_PREFIX$environment-$recordSetId")
                        }
                        index
                    }
                }
                builder.build()
            })

        return try {
            val response = bulkRequest(bulkRequest.build(), 1)
            log.info("Bulk request completed with ${messages.size} messages.")
            handleResponse(response, messages)
        } catch (ex: Exception) {
            handleException(ex, messages)
        }
    }

    private fun bulkRequest(bulkRequest: BulkRequest, attempt: Int): BulkResponse? {
        return try {
            log.info("Sending bulk request. Attempt $attempt")
            client.bulk(bulkRequest)
        } catch (ex: SocketTimeoutException) {
            if (attempt < 6) {
                val delayDuration = (1000 * 2.0.pow(attempt.toDouble())).toLong()
                log.warn("The bulk request timed out. Retrying attempt $attempt after delay of $delayDuration ms")
                Thread.sleep(delayDuration)
                bulkRequest(bulkRequest, attempt + 1)
            } else {
                throw TimeoutException(attempt)
            }
        }
    }

    private fun handleResponse(response: BulkResponse?, messages: List<MessageData>): List<IndexResponseReport> {
        if (response == null) {
            log.error("Bulk request failed: response is null.")
            return messages.map {
                IndexResponseReport(
                    it.key,
                    ReportStatus.fatal,
                    "Bulk request failed: response is null.",
                    it.headers
                )
            }
        }

        val items = mutableMapOf<String, BulkResponseItem>()
        if (response.errors()) {
            log.error(
                ObjectMessage(
                    mapOf(
                        "message" to "Bulk has errors.",
                        "took" to response.took(),
                        "ingestTook" to response.ingestTook()
                    )
                )
            )
            response.items().forEach { item ->
                val id = item.id()
                if (id != null) {
                    items[id] = item
                } else {
                    log.error("Bulk request contains item without id.")
                }
            }
        }

        return messages.map { messageData ->
            val key = messageData.key
            val headers = messageData.headers
            if (!response.errors()) {
                return@map IndexResponseReport(
                    key,
                    ReportStatus.success,
                    "Indexing succeeded.",
                    headers
                )
            }
            val responseItem = items[key]
            if (responseItem == null) {
                log.error("Indexing for $key failed, but could not recover error.")
                return@map IndexResponseReport(
                    key,
                    ReportStatus.fatal,
                    "Indexing failed, but could not recover error.",
                    headers
                )
            }
            val error = responseItem.error()
            val status = when (error) {
                null -> ReportStatus.success
                else -> ReportStatus.fatal
            }
            val message = when (error) {
                null -> {
                    "Indexing succeeded."
                }

                else -> {
                    val causedByData = error.causedBy()
                    val causedBy = if (causedByData != null) {
                        mapOf(
                            "type" to causedByData.type(),
                            "reason" to causedByData.reason(),
                            "stackTrace" to causedByData.stackTrace()
                        )
                    } else {
                        null
                    }

                    val rootCauses = error.rootCause()
                    val rootCauseMaps = rootCauses.map {
                        mapOf(
                            "type" to it.type(),
                            "reason" to it.reason(),
                            "stackTrace" to it.stackTrace(),
                        )
                    }

                    val errorMap = mapOf(
                        "type" to error.type(),
                        "reason" to error.reason(),
                        "stackTrace" to error.stackTrace(),
                        "causedBy" to causedBy,
                        "rootCauses" to rootCauseMaps
                    )

                    log.error(
                        ObjectMessage(
                            mapOf(
                                "message" to "Indexing failed.",
                                "id" to responseItem.id(),
                                "key" to key,
                                "index" to responseItem.index(),
                                "status" to responseItem.status(),
                                "result" to responseItem.result(),
                                "version" to responseItem.version(),
                                "primaryTerm" to responseItem.primaryTerm(),
                                "seqNo" to responseItem.seqNo(),
                                "shard" to responseItem.shards(),
                                "forcedRefresh" to responseItem.forcedRefresh(),
                                "error" to errorMap
                            )
                        )
                    )
                    "Indexing failed. Check the logs for more information."
                }
            }
            IndexResponseReport(key, status, message, headers)
        }
    }

    private fun handleException(ex: Exception, messages: List<MessageData>): List<IndexResponseReport> {
        return when (ex) {
            is TimeoutException -> {
                log.error(
                    ObjectMessage(
                        mapOf(
                            "message" to "Bulk request failed with a timeout exception.",
                            "error" to ex.message,
                            "stacktrace" to ex.stackTraceToString(),
                            "cause" to ex.cause
                        )
                    )
                )
                messages.map {
                    IndexResponseReport(
                        it.key,
                        ReportStatus.fatal,
                        "Bulk request failed with a timeout exception after 5 attempts. Please re-import the data.",
                        it.headers
                    )
                }
            }

            else -> {
                log.error(
                    ObjectMessage(
                        mapOf(
                            "message" to "Bulk request failed with an unknown exception.",
                            "error" to ex.message,
                            "stacktrace" to ex.stackTraceToString(),
                            "cause" to ex.cause
                        )
                    )
                )
                messages.map {
                    IndexResponseReport(
                        it.key,
                        ReportStatus.fatal,
                        "Bulk request failed for an unknown reason: ${ex.message}.",
                        it.headers
                    )
                }
            }
        }
    }
}

