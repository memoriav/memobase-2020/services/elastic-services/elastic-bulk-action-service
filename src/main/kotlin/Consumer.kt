/*
 * Copyright (C) 2020 - present Jonas Waeber
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package ch.memobase

import org.apache.kafka.clients.consumer.ConsumerRecords
import org.apache.kafka.clients.consumer.KafkaConsumer
import org.apache.kafka.common.KafkaException
import org.apache.kafka.common.TopicPartition
import org.apache.logging.log4j.LogManager
import java.time.Duration
import java.util.*
import kotlin.system.exitProcess

class Consumer(private val topic: String, properties: Properties) {
    private val log = LogManager.getLogger(this::class.java)
    private val instance: KafkaConsumer<String, String> = try {
        KafkaConsumer(properties)
    } catch (ex: KafkaException) {
        val message = "Kafka Consumer Error: ${ex.localizedMessage}"
        ex.cause?.localizedMessage.let {
            if (it != null) {
                log.error("$message, which was caused by $it")
            } else {
                log.error(message)
            }
        }
        exitProcess(1)
    }

    init {
        for (p in properties) {
            log.info("Property: ${p.key} - ${p.value}")
        }
    }

    fun subscribe() {
        log.info("Subscribing to $topic.")
        instance.subscribe(listOf(topic))
    }

    fun consume(): ConsumerRecords<String, String> {
        return instance.poll(Duration.ofMillis(100))
    }
}