/*
 * Copyright (C) 2020 - present Jonas Waeber
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package ch.memobase

import org.apache.http.HttpHost
import org.apache.http.ssl.SSLContextBuilder
import org.apache.http.ssl.SSLContexts
import org.apache.logging.log4j.LogManager
import java.nio.file.Files
import java.nio.file.Paths
import java.security.KeyStore
import java.security.cert.Certificate
import java.security.cert.CertificateFactory
import javax.net.ssl.SSLContext
import kotlin.system.exitProcess

class InvalidHostException(message: String) : Exception(message)

private val hostRegex = Regex("^(https?)://(.*):(\\d+)$")
private val log = LogManager.getLogger("ch.memobase.Utility")

fun parseHost(hosts: String): Array<HttpHost> {
    return hosts.split(",").map { host ->
        val hostParts = hostRegex.find(host) ?: throw InvalidHostException("No match for host: $host")
        val scheme = hostParts.groups[1]?.value ?: throw InvalidHostException("No scheme captured for host: $host")
        val hostName = hostParts.groups[2]?.value ?: throw InvalidHostException("No domain captured for host: $host")
        val portGroupValue = hostParts.groups[3]?.value ?: throw InvalidHostException("No port captured for host: $host")
        val port = try {
            portGroupValue.toInt()
        } catch (ex: NumberFormatException) {
            throw InvalidHostException("Port is not a number for host: $host")
        }
        HttpHost(hostName, port, scheme)
    }.toTypedArray()
}

fun buildSSLContext(path: String): SSLContext {
    val caCertificatePath = Paths.get(path)
    val factory = CertificateFactory.getInstance("X.509")
    var trustedCa: Certificate?
    val inputStream = try {
        Files.newInputStream(caCertificatePath)
    } catch (ex: Exception) {
        log.error("Could not open certificate file: $path")
        exitProcess(1)
    }
    inputStream.use { `is` -> trustedCa = factory.generateCertificate(`is`) }
    val trustStore = KeyStore.getInstance("pkcs12")
    trustStore.load(null, null)
    trustStore.setCertificateEntry("ca", trustedCa)
    val sslContextBuilder: SSLContextBuilder = SSLContexts.custom().loadTrustMaterial(trustStore, null)
    return sslContextBuilder.build()
}