/*
 * Copyright (C) 2020 - present Jonas Waeber
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package ch.memobase

import ch.memobase.exceptions.InvalidInputException
import ch.memobase.reporting.ReportStatus
import org.apache.logging.log4j.LogManager
import org.apache.logging.log4j.message.ObjectMessage

class MessageProcessor(
    private val consumer: Consumer,
    private val producer: Producer,
    client: ElasticsearchIndex,
) {
    private val log = LogManager.getLogger(this::class.java)
    private val manager = BulkRequestManager(elastic = client, producer)
    fun process() {
        val records = consumer.consume()
        records.forEach { consumerRecord ->
            if (consumerRecord.key() == null || consumerRecord.key() == "") {
                producer.sendReport(
                    Service.getRandomReportId(),
                    ReportStatus.fatal,
                    "Indexer cannot handle messages without a key, please always supply one!",
                    consumerRecord.headers()
                )
            } else {
                try {
                    manager.add(consumerRecord.key(), consumerRecord.value(), consumerRecord.headers())
                } catch (ex: InvalidInputException) {
                    val key = consumerRecord.key() ?: Service.getRandomReportId()
                    log.error(ObjectMessage(ex))
                    producer.sendReport(
                        key, ReportStatus.fatal, "Invalid input: ${ex.localizedMessage}", consumerRecord.headers()
                    )
                } catch (ex: Exception) {
                    val key = consumerRecord.key() ?: Service.getRandomReportId()
                    log.error(ObjectMessage(ex))
                    producer.sendReport(
                        key,
                        ReportStatus.fatal,
                        "Unknown exception: ${ex.localizedMessage}",
                        consumerRecord.headers()
                    )
                }
            }
        }
    }
}