/*
 * Copyright (C) 2020 - present Jonas Waeber
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package ch.memobase

import ch.memobase.reporting.Report
import org.apache.kafka.clients.producer.KafkaProducer
import org.apache.kafka.clients.producer.ProducerRecord
import org.apache.kafka.common.KafkaException
import org.apache.kafka.common.header.Headers
import org.apache.logging.log4j.LogManager
import java.io.Closeable
import java.util.*
import kotlin.system.exitProcess

class Producer(
    private val reportingTopic: String,
    private val serviceName: String,
    private val version: String,
    producerProps: Properties
) : Closeable {
    private val log = LogManager.getLogger(this::class.java)
    private val instance = try {
        KafkaProducer<String, String>(producerProps)
    } catch (ex: KafkaException) {
        val message = "Kafka Producer Error: ${ex.localizedMessage}"
        ex.cause?.localizedMessage.let {
            if (it != null) {
                log.error("$message, which was caused by $it")
            } else {
                log.error(message)
            }
        }
        exitProcess(1)
    }

    init {
        for (p in producerProps) {
            log.info("Property: ${p.key} - ${p.value}")
        }
    }

    fun sendReport(id: String, status: String, message: String, headers: Headers?) {
        instance.send(
            ProducerRecord(
                reportingTopic, null, id, Report(id, status, message, serviceName, version).toJson(), headers
            )
        )
    }

    override fun close() {
        instance.close()
    }
}
