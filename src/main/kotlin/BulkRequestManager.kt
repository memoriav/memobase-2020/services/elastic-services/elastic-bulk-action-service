/*
 * Copyright (C) 2020 - present Jonas Waeber
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package ch.memobase

import ch.memobase.exceptions.InvalidInputException
import org.apache.kafka.common.header.Headers
import org.apache.logging.log4j.LogManager
import java.util.concurrent.Executors
import java.util.concurrent.ScheduledExecutorService
import java.util.concurrent.ScheduledFuture
import java.util.concurrent.TimeUnit

class BulkRequestManager(
    private val elastic: ElasticsearchIndex,
    private val producer: Producer,
) {
    companion object {
        private const val MAX_BULK_MESSAGES_PER_SEND = 1000
        private const val EMPTY_JSON_OBJECT = "{}"
        private const val QUEUE_FLUSH_DELAY_SECONDS = 5L
    }

    private val log = LogManager.getLogger(this::class.java)

    private val messageQueue = mutableListOf<MessageData>()
    private val scheduler: ScheduledExecutorService = Executors.newSingleThreadScheduledExecutor()
    private var scheduledFuture: ScheduledFuture<*>? = null

    @Synchronized
    fun add(key: String, value: String?, headers: Headers) {
        if (value == null || value == EMPTY_JSON_OBJECT || value.isBlank()) {
            throw InvalidInputException("No data inside of the message for key $key!")
        }
        messageQueue.add(MessageData(key, value, headers))
        if (messageQueue.size >= MAX_BULK_MESSAGES_PER_SEND) {
            log.info("Bulk request limit reached, sending messages to Elasticsearch.")
            send(messageQueue)
            messageQueue.clear()
            log.info("Queue is cleared.")
            scheduledFuture?.cancel(true)
        } else {
            resetQueueFlushTimer()
        }
    }

    private fun resetQueueFlushTimer() {
        scheduledFuture?.cancel(true)
        scheduledFuture = scheduler.schedule(
            {
                synchronized(this) {
                    if (messageQueue.isNotEmpty()) {
                        log.info("Timer expired: sending messages to Elasticsearch.")
                        send(messageQueue)
                        messageQueue.clear()
                        log.info("Queue is cleared after timer ($QUEUE_FLUSH_DELAY_SECONDS s) expired without reaching the limit ($MAX_BULK_MESSAGES_PER_SEND) of messages to send.")
                    }
                }
            },
            QUEUE_FLUSH_DELAY_SECONDS,
            TimeUnit.SECONDS
        )
    }

    private fun send(messages: List<MessageData>) {
        val responses = elastic.bulkIngest(messages)
        responses.forEach { message ->
            producer.sendReport(message.id, message.status, message.message, message.headers)
        }
    }
}
