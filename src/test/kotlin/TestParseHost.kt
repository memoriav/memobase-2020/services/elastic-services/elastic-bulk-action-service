/*
 * Copyright (C) 2020 - present Jonas Waeber
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package ch.memobase

import org.apache.http.HttpHost
import org.assertj.core.api.Assertions.assertThatThrownBy
import org.junit.jupiter.api.Assertions.assertArrayEquals
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance


@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class TestParseHost {

    @Test
    fun testParseHost() {
        val hosts = "https://dd-es01.ub.unibas.ch:8080,https://dd-es02.ub.unibas.ch:8080,https://dd-es03.ub.unibas.ch:8080,https://dd-es04.ub.unibas.ch:8080,https://dd-es05.ub.unibas.ch:8080"
        val result = parseHost(hosts)
        val expected = arrayOf(
            HttpHost("dd-es01.ub.unibas.ch", 8080, "https"),
            HttpHost("dd-es02.ub.unibas.ch", 8080, "https"),
            HttpHost("dd-es03.ub.unibas.ch", 8080, "https"),
            HttpHost("dd-es04.ub.unibas.ch", 8080, "https"),
            HttpHost("dd-es05.ub.unibas.ch", 8080, "https")
        )
        assertArrayEquals(expected, result)
    }

    @Test
    fun testParseHost_InvalidFormat() {
        val hosts = "https://example.com:8080,https://test.com:9090,invalid"
        assertThatThrownBy {
            parseHost(hosts)
        }
    }
}