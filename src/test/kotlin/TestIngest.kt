package ch.memobase

import ch.memobase.Service.Companion.KAFKA_HEADER_KEY_RECORD_SET_ID
import org.apache.kafka.clients.producer.ProducerRecord
import org.junit.jupiter.api.Disabled
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import java.io.File


@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@Disabled
class TestIngest {

    @Test
    fun testIngest() {
        val index = ElasticsearchIndex(
            System.getenv("ES_HOSTS"),
            System.getenv("ES_API_KEY_ID"),
            System.getenv("ES_API_KEY_SECRET"),
            false,
            "",
            "",
            "",
            System.getenv("ES_INDEX"),
            useIngestPipeline = true,
            createDocuments = false,
            environment = "test"
        )


        // create a json string message
        val message = readFile("src/test/resources/message.json")
        val headers = ProducerRecord("topic", "key", "value").headers()
        headers.add(KAFKA_HEADER_KEY_RECORD_SET_ID, "aag-001".toByteArray())
        index.bulkIngest(listOf(MessageData("key", message, headers)))

    }

    private fun readFile(fileName: String): String {
        return File(fileName).readText()
    }
}